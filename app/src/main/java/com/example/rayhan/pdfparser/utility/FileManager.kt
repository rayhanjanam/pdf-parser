package com.example.rayhan.pdfparser.utility

import java.io.File

/**
 * Created by rayhan on 12/9/17.
 */

class FileManager {

    companion object {

        fun findFiles(directory: File, extension: String): ArrayList<File> {
            val foundFiles = ArrayList<File>()

            File(directory.toURI()).walkTopDown().forEach {

                // println("__ ALL FILE __ " + it.absolutePath)

                if (it.isFile && it.absolutePath.endsWith(extension)) {
                    foundFiles.add(it)
                }
            }
            return foundFiles
        }
    }
}