package com.example.rayhan.pdfparser.utility

import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity

/**
 * Created by rayhan on 12/9/17.
 */

class PermissionManager(private val activity: AppCompatActivity) {

    fun checkPermission(permissionString: String, requestCode: Int): Boolean {
        val permissionState = ContextCompat
                .checkSelfPermission(activity, permissionString)

        val permissions = arrayOf(permissionString)

        if (permissionState != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, permissions, requestCode)
            return false
        }

        return true
    }
}