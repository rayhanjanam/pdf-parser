package com.example.rayhan.pdfparser.helper

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.rayhan.pdfparser.R
import kotlinx.android.synthetic.main.row_view.view.*
import java.io.File

/**
 * Created by rayhan on 12/9/17.
 */

class PdfListAdapter(context: Context,
                     private val list: ArrayList<File>) : BaseAdapter() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(position: Int): Any {
        return list[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, view: View?, viewGroup: ViewGroup?): View {

        val rowView: View
        val rowViewHolder: RowViewHolder

        if (view != null) {
            rowView = view
            rowViewHolder = rowView.tag as RowViewHolder
        } else {
            rowView = this.inflater.inflate(R.layout.row_view, viewGroup, false)

            if (rowView == null)
                println("_______ _________ _________ " + list[position].name)
            rowViewHolder = RowViewHolder(rowView)
        }


        rowViewHolder.filename.text = list[position].name
        rowViewHolder.filepath.text = list[position].path

        return rowView
    }

    private class RowViewHolder(rowView: View) {
        val filename: TextView = rowView.tvFilename
        val filepath: TextView = rowView.tvFilepath
    }
}