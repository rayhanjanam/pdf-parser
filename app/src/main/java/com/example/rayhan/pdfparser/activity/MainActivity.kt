package com.example.rayhan.pdfparser.activity

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import com.example.rayhan.pdfparser.R
import com.example.rayhan.pdfparser.helper.launchActivity
import com.example.rayhan.pdfparser.utility.FileManager
import com.example.rayhan.pdfparser.utility.PermissionManager
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File

class MainActivity : AppCompatActivity() {

    private lateinit var swipeLayout: SwipeRefreshLayout
    private lateinit var listView: ListView

    companion object {
        var hasPermission: Boolean = false
        const val RQ_READ_STORAGE = 1100
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        hasPermission = PermissionManager(this)
                .checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE, RQ_READ_STORAGE)

        if (hasPermission) {
            setupSwipeRefresh()
            setupListView()
            setupListClick()
        }
    }

    private fun setupListClick() {
        listView.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            val selectedPdf = listView.adapter.getItem(position) as File

            launchActivity<PdfTextActivity> {
                putExtra(PdfTextActivity.INTENT_PDF, selectedPdf.absolutePath)
            }
        }
    }

    private fun setupSwipeRefresh() {
        swipeLayout = swipeRefreshLayout
        swipeLayout.setOnRefreshListener { setupListView() }
    }

    private fun setupListView() {

        listView = pdf_list

        val pdfFiles = FileManager.findFiles(
                Environment.getExternalStorageDirectory(), ".pdf")

        if (pdfFiles.size > 0) {
            // val listAdapter = PdfListAdapter(this, pdfFiles)
            val listAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, pdfFiles)
            listView.adapter = listAdapter

            if (this.swipeLayout.isRefreshing) {
                this.swipeLayout.isRefreshing = false
            }

        } else {
            Toast.makeText(baseContext, "No Pdf Found", Toast.LENGTH_LONG).show()
            println("__ NO DATA __")
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode <= RQ_READ_STORAGE) {
            hasPermission = grantResults.isNotEmpty()
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
        }
    }
}
