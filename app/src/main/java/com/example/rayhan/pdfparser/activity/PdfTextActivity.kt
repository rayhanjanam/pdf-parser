package com.example.rayhan.pdfparser.activity

import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import com.example.rayhan.pdfparser.R
import com.tom_roush.pdfbox.pdmodel.PDDocument
import com.tom_roush.pdfbox.text.PDFTextStripper
import com.tom_roush.pdfbox.util.PDFBoxResourceLoader
import kotlinx.android.synthetic.main.activity_pdf_text.*
import java.io.File

class PdfTextActivity : AppCompatActivity() {

    companion object {
        const val INTENT_PDF: String = "pdf_file"
    }

    private lateinit var textView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pdf_text)

        textView = tvPdf
        PDFBoxResourceLoader.init(this)

        val pdfFile = File(intent.getStringExtra(INTENT_PDF))
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = pdfFile.name
            actionBar.setDisplayHomeAsUpEnabled(true)
        }

        ConvertToTextAsync(pdfFile).execute()
    }

    inner class ConvertToTextAsync(private val pdfFile: File) :
            AsyncTask<String, String, String>() {

        private lateinit var progressBar: ProgressBar

        override fun onPreExecute() {
            super.onPreExecute()
            progressBar = pgbar
            progressBar.visibility = View.VISIBLE
        }

        override fun doInBackground(vararg p0: String?): String? {
            var result: String? = null

            try {
                val document = PDDocument.load(pdfFile)
                val pdfStripper = PDFTextStripper()
                result = pdfStripper.getText(document)

                document.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return result
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            if (result != null) {
                textView.text = result
                progressBar.visibility = View.GONE
            } else {
                textView.text = "Could not parse the PDF file!"
                progressBar.visibility = View.GONE
            }
        }

    }
}
